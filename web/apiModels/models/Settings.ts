/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Settings = {
  smtp_host: string
  smtp_port: number
  smtp_username: string
  smtp_password: string
  smtp_from: string
  smtp_ssl: string
  library_name: string
  web_url: string
}
